function [lambdaMax,lambdaMin, lambdaMin2] = findLambda3(N)
    %   findLambda find the eigth value to find optimal parameters of the
    %    PAC algorithm according to the number of agent and consumers
    % Fully connected
    
    
    % Nombre de contrainte
    NconstQ = N; %  Q = sum(t) et T=a
    Nconst3 = N * NconstQ; 
    
    %Nombre de variable par agent
  
    sizeQ = 1 + 2 * (N - 1);
    size3 = N * sizeQ;
    
    Gglo = cell(N,1);
    Ghat = zeros(Nconst3,size3);
    B = zeros(N*(N-1), size3);
    
    debutX = 1;
    debutY = 1;
        
    
    for i=(1:N)
        Gglo{i} = zeros(NconstQ,sizeQ);
        Gglo{i}(1,2:N) = -1;
        decalage = 0;
        for j=(1:N)
            if i~=j 
                B((i-1)*(N-1) + j - decalage, (i-1)*sizeQ + j - decalage + N) = 1;
                B((i-1)*(N-1) + j - decalage, (j-1)*sizeQ + i + decalage) = -1;
                Gglo{i}(1 + j - decalage, 1 + j - decalage) = 1;
                Gglo{i}(1 + j - decalage, 1 + j - decalage + N - 1)= 1;
            else 
                decalage = 1;
            end
        end
        offsetX = sizeQ;
        offsetY = NconstQ;
        
        Gglo{i}(1,1) = 1;
        Ghat(debutY:debutY+offsetY-1,debutX:debutX+offsetX-1) =  Gglo{i};
        debutX = debutX + offsetX;
        debutY = debutY + offsetY;
    end

    H = Ghat' * Ghat + B'*B;
    %clear Ghat;
    %clear B;
    lambda = eig(H);
    lambdaMax = max(lambda);
    epsilon = 0.000001;
    lambdahat = lambda(lambda>epsilon);
    lambdaMin = min(lambdahat);
    lambdahat2 = lambda(lambda~=0);
    lambdaMin2 = min(lambdahat2);

end