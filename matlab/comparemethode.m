%% Comparaison selon methode

nMethode = 6;

%methode = ["OSQP", "OSQP2", "OSQP3", "OSQP4"];
methode = ["ADMM", "ADMMMP", "ADMMGPU","PAC", "PACMP", "PACGPU"];
Prefixe = 'MarketJCGE';
%complexite_theorique = [3,3,3,3];
complexite_theorique = [2,2,-1, 3, 3, -1];
Nthreadperblock = 512;
Mat =ComparaisonMarketJCGEall;%ComparaisonMarketCPUReleaseNonConvall;% ComparaisonMarketCPUReleaseAll; %ComparaisonMarketRelease20;% RandomComparaisonIEEESameEpsL;%RandomComparaisonIEEE2;
%methode = [ "ADMMGPU5", "ADMMGPU6","ADMMGPU8","ADMMGPU9"];
%complexite_theorique = 2*[1,1,1,1];
indiceForm = [1, 2, 3, 4, 5];
indiceColor = [1, 2, 3, 4, 5, 6 ];
form = {"x", "o", "s", "d","*", "^", "v",};
ref = 1;  % le temps que le regarde en réference pour la compleité

Param = Mat(1,1:25);
nAgentMax = Param(1);
nNAgent = Param(2);
rhoAgent = Param(3);
epsG = Param(4);
epsL = Param(5);
iterG = Param(6);
iterL = Param(7);
stepG = Param(8);
stepL = Param(9);
nSimu = Param(10);  
if(nMethode ~= Param(11))
    error("wrong number of method")
end
paramCas = Param(12:end); % P, dP, Q, dQ, a, da, b, db, gamma, dgamma; 
% propCons, propPro, propGenNFLE, AC
propCons = Param(22);


Agents_tab = Mat(2,1:nNAgent);
debut = 3;
fin = debut+nMethode*nSimu-1;
temps_all = Mat(debut:fin,1:nNAgent)/1000;
debut = fin+1;
fin = debut+nMethode*nSimu -1;
iter_all =  Mat(debut:fin,1:nNAgent);
debut = fin+1;
fin = debut+nMethode*nSimu -1;
fc_all =  Mat(debut:fin,1:nNAgent);
debut = fin+1;
fin = debut+nMethode*nSimu -1;
ResR_all =  Mat(debut:fin,1:nNAgent);
debut = fin+1;
fin = debut+nMethode*nSimu -1;
ResS_all =  Mat(debut:fin,1:nNAgent);
%%

color = [ "b", "r", "m", "c", "g", "y"];

f = 1;
% Taille de la figure en cm
xSize_r = 18;
ySize_r = 9;
xSize = f*xSize_r;
ySize = f*ySize_r;
% Taille en points : 7
Fontsize_v = f*14;


temps_tab = cell(1,nMethode);
iter_tab = cell(1,nMethode);
temps_iter_tab = cell(1,nMethode);
fc_tab = cell(1,nMethode);
ResR_tab = cell(1,nMethode);
ResS_tab = cell(1,nMethode);

for i =(1:nMethode)
   temps_tab{i} = temps_all((i-1)*nSimu + 1:i*nSimu,:);
   iter_tab{i} = iter_all((i-1)*nSimu + 1:i*nSimu,:);
   temps_iter_tab{i} = temps_tab{i}./iter_tab{i};
   fc_tab{i} = fc_all((i-1)*nSimu + 1:i*nSimu,:);
   ResR_tab{i} = ResR_all((i-1)*nSimu + 1:i*nSimu,:);
   ResS_tab{i} = ResS_all((i-1)*nSimu + 1:i*nSimu,:);
end

%% Analyse temps
temps_moy = zeros(nMethode,nNAgent);
temps_max = zeros(nMethode,nNAgent);
temps_min = zeros(nMethode,nNAgent);
temps_iter_moy = zeros(nMethode,nNAgent);
temps_iter_max = zeros(nMethode,nNAgent);
temps_iter_min = zeros(nMethode,nNAgent);
iter_moy = zeros(nMethode,nNAgent);
iter_max = zeros(nMethode,nNAgent);
iter_min = zeros(nMethode,nNAgent);
timeComparaison = zeros(nMethode,nNAgent);
speedup = zeros(nMethode,nNAgent);
speedup_tab = cell(1,nMethode);
complexity =zeros(nMethode,1);
complexity_iter =zeros(nMethode,1);
complexity_log =zeros(nMethode,1);
eta = zeros(nMethode,1);


factor = nAgentMax/Agents_tab(1);
factor_log = log(nAgentMax)/log(Agents_tab(1));
for i =(1:nMethode)
   temps_moy(i,:) = mean(temps_tab{i});
   temps_max(i,:) = max(temps_tab{i});
   temps_min(i,:) = min(temps_tab{i});
   temps_iter_moy(i,:) = mean(temps_iter_tab{i});
   temps_iter_max(i,:) = max(temps_iter_tab{i});
   temps_iter_min(i,:) = min(temps_iter_tab{i});
   iter_moy(i,:) = mean(iter_tab{i});
   iter_max(i,:) = max(iter_tab{i});
   iter_min(i,:) = min(iter_tab{i});
   speedup_tab{i} = (temps_tab{ref} - temps_tab{i})./temps_tab{ref};
  
end


nFigure =1;
%%
figure('Units','centimeters','Position',[1 2 xSize ySize])
name = strcat(Prefixe,'ComparetempsAgentMethodeRelease');
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])
for i=(1:nMethode)
   plot(Agents_tab,temps_max(i,:), "^" + color{indiceColor(i)},'HandleVisibility','off', LineWidth=1);
   hold on
   plot(Agents_tab,temps_moy(i,:), color{indiceColor(i)}+ "--*" ,'DisplayName', methode{i}, LineWidth=2);%form{indiceForm(i)}
   plot(Agents_tab,temps_min(i,:), "v" + color{indiceColor(i)},'HandleVisibility','off', LineWidth=1); 
end
legend('Location','best');
grid on
title("simulation time (s) according to the agents' count and the method")
axeX = "agents' count";
axeY = "time (s)";
xlabel(axeX,'FontSize',Fontsize_v)
ylabel(axeY,'FontSize',Fontsize_v)
hold off
print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])

%%
figure('Units','centimeters','Position',[1 2 xSize ySize])
name = strcat(Prefixe,'ComparetempsAgentMethodeReleaseLog');
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])
for i=(1:nMethode)
   semilogy(Agents_tab,temps_max(i,:), "^" + color{indiceColor(i)},'HandleVisibility','off', LineWidth=1);
   hold on
   semilogy(Agents_tab,temps_moy(i,:), color{indiceColor(i)}+ "--*" ,'DisplayName', methode{i}, LineWidth=2);%form{indiceForm(i)}
   semilogy(Agents_tab,temps_min(i,:), "v" + color{indiceColor(i)},'HandleVisibility','off', LineWidth=1); 
end
legend('Location','best');
grid on
title("simulation time (s) according to the agents' count and the method")
axeX = "agents' count";
axeY = "time (s)";
xlabel(axeX,'FontSize',Fontsize_v)
ylabel(axeY,'FontSize',Fontsize_v)
hold off
print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])
%%
figure('Units','centimeters','Position',[1 2 xSize ySize])
name = strcat(Prefixe,'CompareIterAgentMethodeRelease');
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])
for i=(1:nMethode)
   plot(Agents_tab,iter_max(i,:), "^" + color{indiceColor(i)},'HandleVisibility','off', LineWidth=1);
   hold on
   plot(Agents_tab,iter_moy(i,:), color{indiceColor(i)}+ "--*" ,'DisplayName', methode{i}, LineWidth=2);%form{indiceForm(i)}
   plot(Agents_tab,iter_min(i,:), "v" + color{indiceColor(i)},'HandleVisibility','off', LineWidth=1); 
end
legend('Location','best');
grid on
title("Iterations according to the agents' count and the method")
axeX = "agents' count";
axeY = "iter ";
xlabel(axeX,'FontSize',Fontsize_v)
ylabel(axeY,'FontSize',Fontsize_v)
hold off
print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])
%%
figure('Units','centimeters','Position',[1 2 xSize ySize])
name = strcat(Prefixe,'CompareTimeIterAgentMethodeRelease');
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])
for i=(1:nMethode)
   plot(Agents_tab,temps_iter_max(i,:), "^" + color{indiceColor(i)},'HandleVisibility','off', LineWidth=1);
   hold on
   plot(Agents_tab,temps_iter_moy(i,:), color{indiceColor(i)}+ "--*" ,'DisplayName', methode{i}, LineWidth=2);%form{indiceForm(i)}
   plot(Agents_tab,temps_iter_min(i,:), "v" + color{indiceColor(i)},'HandleVisibility','off', LineWidth=1); 
end
legend('Location','best');
grid on
title("simulation time (s) by iteration according to the agents' count and the method")
axeX = "agents' count";
axeY = "time (s)";
xlabel(axeX,'FontSize',Fontsize_v)
ylabel(axeY,'FontSize',Fontsize_v)
hold off
print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])

%%
figure('Units','centimeters','Position',[1 2 xSize ySize])
name = strcat(Prefixe,'ComparetempsAgentMethodeReleaseFr');
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])
for i=(1:nMethode)
   plot(Agents_tab,temps_max(i,:), "^" + color{indiceColor(i)},'HandleVisibility','off', LineWidth=1);
   hold on
   plot(Agents_tab,temps_moy(i,:), color{indiceColor(i)}+ "--*" ,'DisplayName', methode{i}, LineWidth=2);%form{indiceForm(i)}
   plot(Agents_tab,temps_min(i,:), "v" + color{indiceColor(i)},'HandleVisibility','off', LineWidth=1); 
end
legend('Location','best');
grid on
title("Temps de simulation (s) selon le nombre d'agent et la méthode")
axeX = "nombre d'agent";
axeY = "temps (s)";
xlabel(axeX,'FontSize',Fontsize_v)
ylabel(axeY,'FontSize',Fontsize_v)
hold off
print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])

%%
figure('Units','centimeters','Position',[1 2 xSize ySize])
name = strcat(Prefixe,'ComparetempsAgentMethodeReleaseLogFr');
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])
for i=(1:nMethode)
   semilogy(Agents_tab,temps_max(i,:), "^" + color{indiceColor(i)},'HandleVisibility','off', LineWidth=1);
   hold on
   semilogy(Agents_tab,temps_moy(i,:), color{indiceColor(i)}+ "--*" ,'DisplayName', methode{i}, LineWidth=2);%form{indiceForm(i)}
   semilogy(Agents_tab,temps_min(i,:), "v" + color{indiceColor(i)},'HandleVisibility','off', LineWidth=1); 
end
legend('Location','best');
grid on
title("Temps de simulation (s) selon le nombre d'agent et la méthode")
axeX = "nombre d'agent";
axeY = "temps (s)";
xlabel(axeX,'FontSize',Fontsize_v)
ylabel(axeY,'FontSize',Fontsize_v)
hold off
print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])
%%
figure('Units','centimeters','Position',[1 2 xSize ySize])
name = strcat(Prefixe,'CompareIterAgentMethodeReleaseFr');
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])
for i=(1:nMethode)
   plot(Agents_tab,iter_max(i,:), "^" + color{indiceColor(i)},'HandleVisibility','off', LineWidth=1);
   hold on
   plot(Agents_tab,iter_moy(i,:), color{indiceColor(i)}+ "--*" ,'DisplayName', methode{i}, LineWidth=2);%form{indiceForm(i)}
   plot(Agents_tab,iter_min(i,:), "v" + color{indiceColor(i)},'HandleVisibility','off', LineWidth=1); 
end
legend('Location','best');
grid on
title("Iteration selon le nombre d'agent et la méthode")
axeX = "nombre d'agent";
axeY = "Iterations ";
xlabel(axeX,'FontSize',Fontsize_v)
ylabel(axeY,'FontSize',Fontsize_v)
hold off
print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])
%%
figure('Units','centimeters','Position',[1 2 xSize ySize])
name = strcat(Prefixe,'CompareTimeIterAgentMethodeReleaseFr');
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])
for i=(1:nMethode)
   plot(Agents_tab,temps_iter_max(i,:), "^" + color{indiceColor(i)},'HandleVisibility','off', LineWidth=1);
   hold on
   plot(Agents_tab,temps_iter_moy(i,:), color{indiceColor(i)}+ "--*" ,'DisplayName', methode{i}, LineWidth=2);%form{indiceForm(i)}
   plot(Agents_tab,temps_iter_min(i,:), "v" + color{indiceColor(i)},'HandleVisibility','off', LineWidth=1); 
end
legend('Location','best');
grid on
title("Temps de simulation (s) par itération selon le nombre d'agent et la méthode")
axeX = "nombre d'agent";
axeY = "temps (s)";
xlabel(axeX,'FontSize',Fontsize_v)
ylabel(axeY,'FontSize',Fontsize_v)
hold off
print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])
%%

for i =(1:nMethode)
   timeComparaison(i,:) = temps_moy(1,:) ./ temps_moy(i,:);
   speedup(i,:) = mean(speedup_tab{i});
   eta(i) = temps_iter_moy(i,nNAgent)/temps_iter_moy(i,ref);
   complexity(i) = log(temps_moy(i,nNAgent)/temps_moy(i,ref))/log(factor);
   complexity_iter(i) = log(eta(i))/log(factor);
   complexity_log(i) = log(eta(i))/log(factor_log);
end
for i =(1:nMethode-1)
    speedup_rel(i,:) = (temps_moy(i,:)- temps_moy(i+1,:))./ temps_moy(i,:); 
end
timeComparaison
speedup
speedup_rel
complexity
complexity_iter
complexity_log

nFigure = nFigure+1;


figure(nFigure)
for i=(1:nMethode)
   semilogy(Agents_tab,timeComparaison(i,:), color{i},'DisplayName', methode{i});
   hold on   
end
 legend()
 title("time ratio according to the problem's size and the method")
 hold off
nFigure = nFigure+1;

figure(nFigure)
for i=(1:nMethode)
   semilogy(Agents_tab,speedup(i,:), color{i},'DisplayName', methode{i});
   hold on   
end
 legend()
 title("SpeedUp according to the problem's size and the method")
 hold off
nFigure = nFigure+1;

 
figure(nFigure)
for i=(1:nMethode)
   semilogy(Agents_tab,temps_iter_max(i,:), "-." + color{i},'HandleVisibility','off');
   hold on
   semilogy(Agents_tab,temps_iter_moy(i,:), color{i},'DisplayName', methode{i});
   semilogy(Agents_tab,temps_iter_min(i,:), "-." + color{i},'HandleVisibility','off'); 
end
legend()
title("time for a step according to the problem's size and the method")
hold off
nFigure = nFigure+1;


figure(nFigure)
for i=(1:nMethode)
   plot(Agents_tab,iter_max(i,:), "-." + color{i},'HandleVisibility','off');
   hold on
   plot(Agents_tab,iter_moy(i,:), color{i},'DisplayName', methode{i});
   plot(Agents_tab,iter_min(i,:), "-." + color{i},'HandleVisibility','off'); 
end
legend()
title("total step count according to the problem's size and the method")
hold off
nFigure = nFigure+1;

%%
figure()
plot(ResR_tab{1}(:),"*")
hold on
plot(ResR_tab{4}(:),"*")

figure()
plot(ResS_tab{1}(:),"*")
hold on
plot(ResS_tab{4}(:),"*")



%%
temps_theorique = zeros(nMethode,nNAgent-1);


for j = (1:nNAgent)
    for i =(1:nMethode)
       if complexite_theorique(i)>0
            tempsParOperation= mean(temps_moy(i,:)'./power(Agents_tab(:),complexite_theorique(i)));
            temps_theorique(i,j) = tempsParOperation*power(Agents_tab(j),complexite_theorique(i));
       else
           tempsParOperation= mean(temps_moy(i,:)'./(Agents_tab(:)/Nthreadperblock + log(Agents_tab(:))));
           temps_theorique(i,j) = tempsParOperation*(Agents_tab(j)/Nthreadperblock + log(Agents_tab(j)));
       end
       
   end
end

figure(nFigure)
for i=(1:nMethode)
   semilogy(Agents_tab,temps_moy(i,:), "-.b" ,'DisplayName', methode{i});
   hold on
   semilogy(Agents_tab,temps_theorique(i,:), "--*r",'DisplayName', methode{i}+" theo");
end
legend()
title("Increase of simulation time according to the problem size ")
hold off
nFigure = nFigure+1;

