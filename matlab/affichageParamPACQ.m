%% Afficher les valeurs de lambaMa ou lambdaMin selon le nombre d'agent 
% et de consommateur
color = [ "b", "r", "m", "c", "g"];

f = 1;
% Taille de la figure en cm
xSize_r = 18;
ySize_r = 9;
xSize = f*xSize_r;
ySize = f*ySize_r;
% Taille en points : 7
Fontsize_v = f*14;

%%
%float alpha = 1 * a.min2(); avec a coefficient directeur des fonctions
%coûts
%float L = 1 * a.max2();

nNagent = 7;
nPropConso = 17;

Nagent = floor(linspace(10, 40, nNagent));
propConso = linspace(0.1, 0.9, nPropConso);
nConso = zeros(nNagent, nPropConso);


lambdaMaxAll = zeros(nNagent, nPropConso);
lambdaMinAll= zeros(nNagent, nPropConso);

for i=(1:nNagent)
    i
    for j=(1:nPropConso)
        j
        nConso(i,j) = floor(Nagent(i) * propConso(j));
        [lambdaMax, lambdaMin, lambdaMin2] = findLambda2(Nagent(i), nConso(i,j));
        lambdaMaxAll(i,j) = lambdaMax;
        lambdaMinAll(i,j) = lambdaMin;
    end
end

%nGen = Nagent-nConso;
%% Affichage



figure()
surface(propConso,Nagent , lambdaMaxAll, 'LineStyle','none', 'EdgeColor','flat');
colorbar();
view(3)


figure()
surface(propConso, Nagent , lambdaMinAll, 'LineStyle','none', 'EdgeColor','flat');
colorbar();
view(3)




%% 
figure()
moitie = floor(nPropConso/2);
for j=(1:moitie)
    plot(Nagent, lambdaMaxAll(:,j), "DisplayName", "prop" + num2str(propConso(j)), "LineStyle","--","Marker","o");
    hold on
end
for j =(moitie+1:nPropConso)
   plot(Nagent, lambdaMaxAll(:,j), "DisplayName", "prop" + num2str(propConso(j)), "LineStyle","-.","Marker","+");
   hold on
end
legend();

figure()
moitie = floor(nNagent/2);
for j=(1:moitie)
    plot(propConso, lambdaMaxAll(j,:), "DisplayName", "nAgent " + num2str(Nagent(j)), "LineStyle","--","Marker","o");
    hold on
end
for j =(moitie+1:nNagent)
   plot(propConso, lambdaMaxAll(j,:), "DisplayName", "nAgent " + num2str(Nagent(j)), "LineStyle","-.","Marker","+");
   hold on
end
legend();
grid on
%% 
figure()
moitie = floor(nPropConso/2);
for j=(1:moitie)
    plot(Nagent, lambdaMinAll(:,j), "DisplayName", "prop" + num2str(propConso(j)), "LineStyle","--","Marker","o");
    hold on
end
for j =(moitie+1:nPropConso)
   plot(Nagent, lambdaMinAll(:,j), "DisplayName", "prop" + num2str(propConso(j)), "LineStyle","-.","Marker","+");
   hold on
end
legend();

figure()
moitie = floor(nNagent/2);
for j=(1:moitie)
    plot(propConso, lambdaMinAll(j,:), "DisplayName", "nAgent " + num2str(Nagent(j)), "LineStyle","--","Marker","o");
    hold on
end
for j =(moitie+1:nNagent)
   plot(propConso, lambdaMinAll(j,:), "DisplayName", "nAgent " + num2str(Nagent(j)), "LineStyle","-.","Marker","+");
   hold on
end
legend();
grid on

%%
figure('Units','centimeters','Position',[1 2 xSize ySize])
name = strcat('UtilitairePACQ','lambdaMaxAll');
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])
moitie = floor(nNagent/2) + 1;
for j=(1:moitie)
    plot(nConso(j,:), lambdaMaxAll(j,:), "DisplayName", "nAgent " + num2str(Nagent(j)), "LineStyle","--","Marker","o");
    hold on
end
for j =(moitie+1:nNagent)
   plot(nConso(j,:), lambdaMaxAll(j,:), "DisplayName", "nAgent " + num2str(Nagent(j)), "LineStyle","-.","Marker","+");
   hold on
end
legend("Location","best");
title("Valeur propre maximale ");
grid on
axeX = "Nombre de consomateur";
axeY = "Plus grande valeur propre";
xlabel(axeX,'FontSize',Fontsize_v)
ylabel(axeY,'FontSize',Fontsize_v)
hold off

print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])



figure('Units','centimeters','Position',[1 2 xSize ySize])
name = strcat('UtilitairePACQ','lambdaMinAll');
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])

moitie = floor(nNagent/2) + 1;
for j=(1:moitie)
    plot(nConso(j,:), lambdaMinAll(j,:), "DisplayName", "nAgent " + num2str(Nagent(j)), "LineStyle","--","Marker","o");
    hold on
end
for j =(moitie+1:nNagent)
   plot(nConso(j,:), lambdaMinAll(j,:), "DisplayName", "nAgent " + num2str(Nagent(j)), "LineStyle","-.","Marker","+");
   hold on
end
title("Valeur propre minimale  ");
legend("Location","best");
axeX = "Nombre de consomateur";
axeY = "Plus petite valeur propre";
xlabel(axeX,'FontSize',Fontsize_v)
ylabel(axeY,'FontSize',Fontsize_v)
hold off
print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])



%% Analyse via nConso lambdaMaxAll
% Ce sont des constantes autant juste prendre la première valeur
% Cette constant varie linéairement avec Nagent 


Const = lambdaMaxAll(:,1);
CoefDirectConst = (Const(nNagent) - Const(1))/(Nagent(nNagent) - Nagent(1));
CoefConstConst = Const(1) - Nagent(1) * CoefDirectConst;


figure()
plot(Nagent, Const, "DisplayName","valeur propre Max")
hold on
plot(Nagent, CoefConstConst + CoefDirectConst * Nagent, "DisplayName","valeur propre Max appro", "LineStyle","--", Marker="o")
legend()


%% Analyse via nConso lambdaMinAll
% Ce sont des constantes autant juste prendre la première valeur
% Cette constant varie en 1/Nagent 


Const2 = lambdaMinAll(:,1);
LN = Const2'.*Nagent;
N2 = Nagent.*Nagent;
LN2 = LN.*Nagent;
coef1Excel = 1.9927;
coef2Excel = -3.3289;
LNappro = coef1Excel + coef2Excel./Nagent; %excel
%CoefDirectConst2 = (Const2(nNagent) - Const2(1))/(Nagent(nNagent) - Nagent(1));
%CoefConstConst2 = Const2(1) - Nagent(1) * CoefDirectConst2;

coefDireLN = (LN(nNagent) - LN(1))./ (Nagent(nNagent) - Nagent(1));
termeConstLN = LN(1) - coefDireLN .* Nagent(1);
coefDireLN2 = (LN2(nNagent) - LN2(1))./ (Nagent(nNagent) - Nagent(1));
termeConstLN2 = LN2(1) - coefDireLN2 .* Nagent(1);
LNappro2 = coefDireLN2+termeConstLN2./Nagent; %matlab



figure()
plot(Nagent, Const2, "DisplayName","valeur propre Min")
hold on
plot(Nagent, coefDireLN + termeConstLN./Nagent, "LineStyle","--", Marker="o", DisplayName="a+b/N")
plot(Nagent, LNappro./Nagent, "LineStyle","--", Marker="o", DisplayName="(c+d/N)/N")
plot(Nagent, LNappro2./Nagent, "LineStyle","--", Marker="o", DisplayName="(c+d/N)/N version 2")

legend()



figure()
plot(Nagent, LN, "DisplayName","C * Nagent")
hold on

legend()

figure()
plot(Nagent, LN2, "DisplayName","C * Nagent* Nagent","LineStyle","--", Marker="o")
hold on 
plot(Nagent, coefDireLN2.*Nagent+termeConstLN2, "DisplayName","appro")
legend()
%% Resultat

ApproLambdaMax = zeros(nNagent, nPropConso);
ApproLambdaMin = zeros(nNagent, nPropConso);

for i=(1:nNagent)
    for j=(1:nPropConso)
        N = Nagent(i);
        Y = nConso(i,j);

         
        ApproLambdaMax(i,j) = CoefDirectConst * N + CoefConstConst;
        ApproLambdaMin(i,j) = (coef1Excel + coef2Excel./N)/N;

    end
end

ErrLambdaMax = lambdaMaxAll - ApproLambdaMax;
ErrLambdaMin = lambdaMinAll - ApproLambdaMin;

figure('Units','centimeters','Position',[1 2 xSize ySize])
name = strcat('UtilitairePACQ','lambdaMaxAllAppro');
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])
moitie = floor(nNagent/2) + 1;
for j=(1:moitie)
    plot(nConso(j,:), ApproLambdaMax(j,:), "DisplayName", "nAgent " + num2str(Nagent(j)), "LineStyle","--","Marker","o");
    hold on
end
for j =(moitie+1:nNagent)
   plot(nConso(j,:), ApproLambdaMax(j,:), "DisplayName", "nAgent " + num2str(Nagent(j)), "LineStyle","-.","Marker","+");
   hold on
end
legend("Location","best");
title("Valeur propre maximale approximée ");
grid on
axeX = "Nombre de consomateur";
axeY = "Plus grande valeur propre ";
xlabel(axeX,'FontSize',Fontsize_v)
ylabel(axeY,'FontSize',Fontsize_v)
hold off

print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])
figure('Units','centimeters','Position',[1 2 xSize ySize])
name = strcat('UtilitairePACQ','lambdaMinAllAppro');
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])
moitie = floor(nNagent/2) + 1;
for j=(1:moitie)
    plot(nConso(j,:), ApproLambdaMin(j,:), "DisplayName", "nAgent " + num2str(Nagent(j)), "LineStyle","--","Marker","o");
    hold on
end
for j =(moitie+1:nNagent)
   plot(nConso(j,:), ApproLambdaMin(j,:), "DisplayName", "nAgent " + num2str(Nagent(j)), "LineStyle","-.","Marker","+");
   hold on
end
legend("Location","best");
title("Valeur propre minimale approximée");
grid on
axeX = "Nombre de consomateur";
axeY = "Plus petite valeur propre";
xlabel(axeX,'FontSize',Fontsize_v)
ylabel(axeY,'FontSize',Fontsize_v)
hold off

print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])

max(abs(ErrLambdaMin), [], "all")
max(abs(ErrLambdaMax), [], "all")



