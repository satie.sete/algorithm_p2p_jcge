%% Afficher les valeurs de lambaMa ou lambdaMin selon le nombre d'agent 
% et de consommateur
color = [ "b", "r", "m", "c", "g"];

f = 1;
% Taille de la figure en cm
xSize_r = 18;
ySize_r = 9;
xSize = f*xSize_r;
ySize = f*ySize_r;
% Taille en points : 7
Fontsize_v = f*14;

%%
%float alpha = 1 * a.min2(); avec a coefficient directeur des fonctions
%coûts
%float L = 1 * a.max2();

nNagent = 17;
nPropConso = 1;

Nagent = floor(linspace(10, 90, nNagent));
%propConso = linspace(0.1, 0.9, nPropConso);
propConso = zeros(1,nNagent);
nConso = 1;


lambdaMaxAll = zeros(1,nNagent);
lambdaMinAll= zeros(1,nNagent);

for i=(1:nNagent)
    propConso(i) = nConso/Nagent(i);
    [lambdaMax, lambdaMin, lambdaMin2] = findLambda(Nagent(i), nConso);
    lambdaMaxAll(i) = lambdaMax;
    lambdaMinAll(i) = lambdaMin;
end


%% 
figure('Units','centimeters','Position',[1 2 xSize ySize])
name = strcat('UtilitairePAC1Conso','lambdaMaxAll');
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])
plot(Nagent, lambdaMaxAll, "DisplayName", "1 conso", "LineStyle","--","Marker","o");
legend();
legend("Location","best");
title("Valeur propre maximale ");
grid on
axeX = "Nombre d'agent";
axeY = "Plus grande valeur propre";
xlabel(axeX,'FontSize',Fontsize_v)
ylabel(axeY,'FontSize',Fontsize_v)
hold off

print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])

figure('Units','centimeters','Position',[1 2 xSize ySize])
name = strcat('UtilitairePAC1Conso','lambdaMinAll');
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])
plot(Nagent, lambdaMinAll, "DisplayName", "1 conso", "LineStyle","--","Marker","o");
legend();
title("Valeur propre minimale  ");
legend("Location","best");
axeX = "Nombre d'agent";
axeY = "Plus petite valeur propre";
xlabel(axeX,'FontSize',Fontsize_v)
ylabel(axeY,'FontSize',Fontsize_v)
hold off
print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])



%% Analyse lambdaMaxAll
% C'est complètement linéaire

penteA = (lambdaMaxAll(nNagent) - lambdaMaxAll(1))./ (Nagent(nNagent) - Nagent(1));

termeConstA = lambdaMaxAll(1) - penteA .* Nagent(1);




figure()
plot(Nagent, penteA*Nagent + termeConstA, "DisplayName","valeur propre Max appro", "LineStyle","--", Marker="o")
hold on
plot(Nagent, lambdaMaxAll, "DisplayName","valeur propre Max")
legend()


%% Analyse via nConso lambdaMinAll
% c'est une forme en a + b/N car *N c'est lineaire

LN = lambdaMinAll.*Nagent;
coefDireLN = (LN(nNagent) - LN(1))./ (Nagent(nNagent) - Nagent(1));
termeConstLN = LN(1) - coefDireLN .* Nagent(1);

figure()
plot(Nagent, lambdaMinAll, "DisplayName","valeur propre Min")
hold on
plot(Nagent, coefDireLN + termeConstLN./Nagent, "LineStyle","--", Marker="o", DisplayName="valeur propre Min appro")
legend()



figure()
plot(Nagent, LN, "DisplayName","C * Nagent")
hold on
legend()




%% Resultat

ApproLambdaMax = zeros(1,nNagent);
ApproLambdaMin = zeros(1,nNagent);

for i=(1:nNagent) 
    N = Nagent(i);
    ApproLambdaMax(i) = penteA * N + termeConstA;
    ApproLambdaMin(i) = coefDireLN + termeConstLN/N;
end

ErrLambdaMax = lambdaMaxAll - ApproLambdaMax;
ErrLambdaMin = lambdaMinAll - ApproLambdaMin;


max(abs(ErrLambdaMin), [], "all")
max(abs(ErrLambdaMax), [], "all")



