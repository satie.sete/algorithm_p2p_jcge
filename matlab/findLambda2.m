function [lambdaMax,lambdaMin, lambdaMin2] = findLambda2(N,Ncons)
    %   findLambda find the eigth value to find optimal parameters of the
    %    PAC algorithm according to the number of agent and consumers
    Ncons = floor(Ncons);
    Ngen = N - Ncons;
    
    % Nombre de contrainte
    Nconst1 = 1 + Ngen; % P = sum(T) et T=a
    Nconst2 = 1 + Ncons; %  P = sum(T) et T=a
    NconstQ = N; %  Q = sum(t) et T=a
    Nconst3 = Ngen * Nconst2 + Ncons * Nconst1 + N * NconstQ; 
    
    %Nombre de variable par agent
    size1 = 1 + 2 * Ngen;
    size2 = 1 + 2 * Ncons;
    sizeQ = 1 + 2 * (N -1);
    size3 = Ncons * size1 + Ngen * size2 + N * sizeQ;
    
    Gglo = cell(2*N,1);
    Ghat = zeros(Nconst3,size3);
    B = zeros(2*Ncons*Ngen + N*(N-1), size3);
    
    debutX = 1;
    debutY = 1;
        
    for i=(1:N)
        if(i<=Ncons)
            Gglo{i} = zeros(Nconst1,size1);
            Gglo{i}(1,2:1+Ngen) = -1;
            for j=(1:Ngen)
                B((i-1)*Ngen + j, debutX + j + Ngen) = 1;
                B((i-1)*Ngen + j, Ncons*size1 + (j - 1)*size2 + i + 1) = -1;
                Gglo{i}(1 + j, 1 + j) = 1;
                Gglo{i}(1 + j, 1 + j + Ngen)= 1;
            end
            offsetX = size1;
            offsetY = Nconst1;
        else
            Gglo{i} = zeros(Nconst2,size2);
            Gglo{i}(1,2:1 + Ncons) = -1;
            for j=(1:Ncons)
                 B(Ncons * Ngen + (i - Ncons -1)*Ncons + j, debutX + j + Ncons) = 1;
                 B(Ncons * Ngen + (i - Ncons -1)*Ncons + j,(j - 1) * size1 + (i - Ncons) + 1 ) = -1;
                 Gglo{i}(1 + j, 1 + j ) = 1;
                 Gglo{i}(1 + j, 1 + j + Ncons)= 1;
            end
            offsetX = size2;
            offsetY = Nconst2;
        end
        Gglo{i}(1,1) = 1;
        
    
    
        Ghat(debutY:debutY+offsetY-1,debutX:debutX+offsetX-1) =  Gglo{i};
        debutX = debutX + offsetX;
        debutY = debutY + offsetY;
    end
    for i=(N+1:2*N)
        
        Gglo{i} = zeros(NconstQ,sizeQ);
        Gglo{i}(1,2:N) = -1;
        decalage = 0;
        for j=(1:N)
            if i~=(j + N)
                varP = Ncons * size1 +  Ngen * size2;
                B(2*Ngen*Ncons + (i-N-1)*(N-1) + j - decalage, varP + (i-N-1)*sizeQ + j - decalage + N) = 1;
                B(2*Ngen*Ncons + (i-N-1)*(N-1) + j - decalage, varP + (j-1)*sizeQ + (i - N - (1-decalage)) + 1) = -1;
                Gglo{i}(1 + j - decalage, 1 + j - decalage) = 1;
                Gglo{i}(1 + j - decalage, 1 + j - decalage + N - 1)= 1;
            else 
                decalage = 1;
            end
        end
        offsetX = sizeQ;
        offsetY = NconstQ;
        
        Gglo{i}(1,1) = 1;
        
    
    
        Ghat(debutY:debutY+offsetY-1,debutX:debutX+offsetX-1) =  Gglo{i};
        debutX = debutX + offsetX;
        debutY = debutY + offsetY;
    end

    H = Ghat' * Ghat + B'*B;
    %clear Ghat;
    %clear B;
    lambda = eig(H);
    lambdaMax = max(lambda);
    epsilon = 0.000001;
    lambdahat = lambda(lambda>epsilon);
    lambdaMin = min(lambdahat);
    lambdahat2 = lambda(lambda~=0);
    lambdaMin2 = min(lambdahat2);

end