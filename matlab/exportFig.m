function [] = exportFig(X,Y,Legend,titre,axeX,axeY, name,Localisation)

f = 1;
% Taille de la figure en cm
xSize_r = 18;
ySize_r = 9;
xSize = f*xSize_r;
ySize = f*ySize_r;
% Taille en points : 7
Fontsize_v = f*14;
colors = {"m--*", "c--*", "r--*", "g--*", "b--*", "k--*", "y--*","m--o"};
figure('Units','centimeters','Position',[1 2 xSize ySize])
set(gcf, 'Color', 'w');

dimY = size(Y);
dimX = size(X);
assert(dimY(2)==dimX(2));

if dimX(1)==1
    for i =1:dimY(1)
        plot(X,Y(i,:),colors{i},"DisplayName",Legend{i},'LineWidth',f);
        hold on
    end 
else
    for i =1:dimY(1)
        plot(X(i,:),Y(i,:),colors{i}+"--*","DisplayName",Legend{i},'LineWidth',f);
        hold on
    end 
end

xlabel(axeX,'FontSize',Fontsize_v)
ylabel(axeY,'FontSize',Fontsize_v)
legend('Location',Localisation);
grid on
title(titre)

set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])

print2eps(name)

eps2pdf([name,'.eps'], [name,'.pdf'])
end

