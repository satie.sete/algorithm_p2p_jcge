%% Afficher les valeurs de lambaMa ou lambdaMin selon le nombre d'agent 
% et de consommateur
color = [ "b", "r", "m", "c", "g"];

f = 1;
% Taille de la figure en cm
xSize_r = 18;
ySize_r = 9;
xSize = f*xSize_r;
ySize = f*ySize_r;
% Taille en points : 7
Fontsize_v = f*14;

%%
%float alpha = 1 * a.min2(); avec a coefficient directeur des fonctions
%coûts
%float L = 1 * a.max2();

nNagent = 17;


Nagent = floor(linspace(10, 90, nNagent));



lambdaMaxAll = zeros(1,nNagent);
lambdaMinAll= zeros(1,nNagent);

for i=(1:nNagent)
    [lambdaMax, lambdaMin, lambdaMin2] = findLambda3(Nagent(i));
    lambdaMaxAll(i) = lambdaMax;
    lambdaMinAll(i) = lambdaMin;
end


%% 
figure('Units','centimeters','Position',[1 2 xSize ySize])
name = strcat('UtilitairePACQueQ','lambdaMaxAll');
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])
plot(Nagent, lambdaMaxAll, "DisplayName", "Fully-Connected", "LineStyle","--","Marker","o");
legend();
legend("Location","best");
title("Valeur propre maximale ");
grid on
axeX = "Nombre d'agent";
axeY = "Plus grande valeur propre";
xlabel(axeX,'FontSize',Fontsize_v)
ylabel(axeY,'FontSize',Fontsize_v)
hold off

print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])

figure('Units','centimeters','Position',[1 2 xSize ySize])
name = strcat('UtilitairePACQueQ','lambdaMinAll');
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])
plot(Nagent, lambdaMinAll, "DisplayName", "Fully-Connected", "LineStyle","--","Marker","o");
legend();
title("Valeur propre minimale  ");
legend("Location","best");
axeX = "Nombre d'agent";
axeY = "Plus petite valeur propre";
xlabel(axeX,'FontSize',Fontsize_v)
ylabel(axeY,'FontSize',Fontsize_v)
hold off
print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])


%% Analyse lambdaMaxAll
% C'est complètement linéaire 

penteA = (lambdaMaxAll(nNagent) - lambdaMaxAll(1))./ (Nagent(nNagent) - Nagent(1));

termeConstA = lambdaMaxAll(1) - penteA .* Nagent(1);




figure()
plot(Nagent, penteA*Nagent + termeConstA, "DisplayName","valeur propre Max appro", "LineStyle","--", Marker="o")
hold on
plot(Nagent, lambdaMaxAll, "DisplayName","valeur propre Max")
legend()
%% Analyse via nConso lambdaMinAll
% c'est une forme en b/N +c/N^2 car *N^2 c'est lineaire (terme restant trop
% dur à déterminer pour une erreur toute petite
% la version excel a l'air plus précise

LN = lambdaMinAll.*Nagent;
coef1Excel = 1.9927;
coef2Excel = -3.3289;
LNappro = coef1Excel + coef2Excel./Nagent; %excel
N2 = Nagent.*Nagent;
LN2 = LN.*Nagent;
coefDireLN = (LN(nNagent) - LN(1))./ (Nagent(nNagent) - Nagent(1));
termeConstLN = LN(1) - coefDireLN .* Nagent(1);
coefDireLN2 = (LN2(nNagent) - LN2(1))./ (Nagent(nNagent) - Nagent(1));
termeConstLN2 = LN2(1) - coefDireLN2 .* Nagent(1);
LNappro2 = coefDireLN2+termeConstLN2./Nagent; %matlab

figure()
plot(Nagent, lambdaMinAll, "DisplayName","valeur propre Min")
hold on
plot(Nagent, coefDireLN + termeConstLN./Nagent, "LineStyle","--", Marker="o", DisplayName="a+b/N")
plot(Nagent, LNappro./Nagent, "LineStyle","--", Marker="o", DisplayName="(c+d/N)/N")
plot(Nagent, LNappro2./Nagent, "LineStyle","--", Marker="o", DisplayName="(c+d/N)/N version 2")

legend()



figure()
plot(Nagent, LN, "DisplayName","C * Nagent")
hold on
plot(Nagent, 1.92-log(Nagent)./Nagent, "DisplayName","log(Nagent)/Nagent")

legend()

figure()
plot(Nagent, LN2, "DisplayName","C * Nagent* Nagent","LineStyle","--", Marker="o")
hold on 
plot(Nagent, coefDireLN2.*Nagent+termeConstLN2, "DisplayName","appro")
legend()

% figure()
% plot(Nagent,LN-LNappro,"DisplayName", "err sur LN")
% 
% figure()
% plot(Nagent,LN2- LNappro.*Nagent,"DisplayName", "err sur LN2")
% hold on
% plot(Nagent, 50./N2 + Nagent.*log(Nagent)/1000 -0.25, "DisplayName","test")
% legend()

%% Resultat

ApproLambdaMax = zeros(1,nNagent);
ApproLambdaMin = zeros(1,nNagent);

for i=(1:nNagent) 
    N = Nagent(i);
    ApproLambdaMax(i) = penteA * N + termeConstA;
    ApproLambdaMin(i) = (coef1Excel + coef2Excel./N)/N;
end

ErrLambdaMax = lambdaMaxAll - ApproLambdaMax;
ErrLambdaMin = lambdaMinAll - ApproLambdaMin;


max(abs(ErrLambdaMin), [], "all")
max(abs(ErrLambdaMax), [], "all")

