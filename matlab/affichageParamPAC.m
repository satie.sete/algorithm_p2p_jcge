%% Afficher les valeurs de lambaMa ou lambdaMin selon le nombre d'agent 
% et de consommateur
color = [ "b", "r", "m", "c", "g"];

f = 1;
% Taille de la figure en cm
xSize_r = 18;
ySize_r = 9;
xSize = f*xSize_r;
ySize = f*ySize_r;
% Taille en points : 7
Fontsize_v = f*14;

%%
%float alpha = 1 * a.min2(); avec a coefficient directeur des fonctions
%coûts
%float L = 1 * a.max2();

nNagent = 9; % 
nPropConso = 3;

Nagent = floor(linspace(2, 10, nNagent));
propConso = linspace(0.25, 0.75, nPropConso);
%propConso = 0;
nConso = zeros(nNagent, nPropConso);


lambdaMaxAll = zeros(nNagent, nPropConso);
lambdaMinAll= zeros(nNagent, nPropConso);

for i=(1:nNagent)
    i
    for j=(1:nPropConso)
        j
        nConso(i,j) = floor(Nagent(i) * propConso(j));
        
        [lambdaMax, lambdaMin, lambdaMin2] = findLambda(Nagent(i), nConso(i,j));
        lambdaMaxAll(i,j) = lambdaMax;
        lambdaMinAll(i,j) = lambdaMin;
    end
end

%% Affichage



figure()
surface(propConso,Nagent , lambdaMaxAll, 'LineStyle','none', 'EdgeColor','flat');
colorbar();
view(3)


figure()
surface(propConso, Nagent , lambdaMinAll, 'LineStyle','none', 'EdgeColor','flat');
colorbar();
view(3)




%% 
figure()
moitie = floor(nPropConso/2);
for j=(1:moitie)
    plot(Nagent, lambdaMaxAll(:,j), "DisplayName", "prop = " + num2str(propConso(j)), "LineStyle","--","Marker","o");
    hold on
end
for j =(moitie+1:nPropConso)
   plot(Nagent, lambdaMaxAll(:,j), "DisplayName", "prop = " + num2str(propConso(j)), "LineStyle","-.","Marker","+");
   hold on
end
legend();

figure()
moitie = floor(nNagent/2);
for j=(1:moitie)
    plot(propConso, lambdaMaxAll(j,:), "DisplayName", "nAgent " + num2str(Nagent(j)), "LineStyle","--","Marker","o");
    hold on
end
for j =(moitie+1:nNagent)
   plot(propConso, lambdaMaxAll(j,:), "DisplayName", "nAgent " + num2str(Nagent(j)), "LineStyle","-.","Marker","+");
   hold on
end
legend();
grid on

%% 
figure()
moitie = floor(nPropConso/2);
for j=(1:moitie)
    plot(Nagent, lambdaMinAll(:,j), "DisplayName", "prop = " + num2str(propConso(j)), "LineStyle","--","Marker","o");
    hold on
end
for j =(moitie+1:nPropConso)
   plot(Nagent, lambdaMinAll(:,j), "DisplayName", "prop = " + num2str(propConso(j)), "LineStyle","-.","Marker","+");
   hold on
end
legend();

figure()
moitie = floor(nNagent/2);
for j=(1:moitie)
    plot(propConso, lambdaMinAll(j,:), "DisplayName", "nAgent " + num2str(Nagent(j)), "LineStyle","--","Marker","o");
    hold on
end
for j =(moitie+1:nNagent)
   plot(propConso, lambdaMinAll(j,:), "DisplayName", "nAgent " + num2str(Nagent(j)), "LineStyle","-.","Marker","+");
   hold on
end
legend();
grid on

%%
figure('Units','centimeters','Position',[1 2 xSize ySize])
name = strcat('UtilitairePAC','lambdaMaxAll');
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])
moitie = floor(nNagent/2) + 1;
for j=(1:moitie)
    plot(nConso(j,:), lambdaMaxAll(j,:), "DisplayName", "nAgent " + num2str(Nagent(j)), "LineStyle","--","Marker","o");
    hold on
end
for j =(moitie+1:nNagent)
   plot(nConso(j,:), lambdaMaxAll(j,:), "DisplayName", "nAgent " + num2str(Nagent(j)), "LineStyle","-.","Marker","+");
   hold on
end
legend("Location","best");
title("Valeur propre maximale ");
grid on
axeX = "Nombre de consomateur";
axeY = "Plus grande valeur propre";
xlabel(axeX,'FontSize',Fontsize_v)
ylabel(axeY,'FontSize',Fontsize_v)
hold off

print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])



figure('Units','centimeters','Position',[1 2 xSize ySize])
name = strcat('UtilitairePAC','lambdaMinAll');
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])

moitie = floor(nNagent/2) + 1;
for j=(1:moitie)
    plot(nConso(j,:), lambdaMinAll(j,:), "DisplayName", "nAgent " + num2str(Nagent(j)), "LineStyle","--","Marker","o");
    hold on
end
for j =(moitie+1:nNagent)
   plot(nConso(j,:), lambdaMinAll(j,:), "DisplayName", "nAgent " + num2str(Nagent(j)), "LineStyle","-.","Marker","+");
   hold on
end
title("Valeur propre minimale  ");
legend("Location","best");
axeX = "Nombre de consomateur";
axeY = "Plus petite valeur propre";
xlabel(axeX,'FontSize',Fontsize_v)
ylabel(axeY,'FontSize',Fontsize_v)
hold off
print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])

%% Analyse
% moitie = floor(nPropConso/2) + 1;
% penteProp(:)   = (lambdaMaxAll(nNagent,:) - lambdaMaxAll(1,:)) ./(Nagent(nNagent) - Nagent(1));
% penteNagent(:) = (lambdaMaxAll(:,moitie-1) - lambdaMaxAll(:,1))./(propConso(moitie-1) - propConso(1));
% 
% termeConst(:) = lambdaMaxAll(1,:) - penteProp * Nagent(1);
% 
% figure()
% 
% plot(propConso(moitie) - propConso(1:moitie), penteProp(1:moitie));
% hold on 
% plot(propConso(moitie+1:nPropConso) - propConso(moitie), penteProp(moitie+1:nPropConso));
% 
% figure()
% plot(propConso(moitie) - propConso(1:moitie), termeConst(1:moitie));
% hold on 
% plot(propConso(moitie+1:nPropConso) - propConso(moitie), termeConst(moitie+1:nPropConso));

%% Analyse via nConso lambdaMaxAll
% 2 phases A et B, A pour y<0.5 et B pour y>0.5, y=0.5 appartient presque 
% aux 2. Attention 0.5 ne tombe pas juste quand le nombre d'agent est
% impair !!! 
moitie = floor(nPropConso/2) + 1;
penteA = (lambdaMaxAll(:,moitie-1) - lambdaMaxAll(:,1))./ (nConso(:,moitie-1) - nConso(:,1));
penteB = (lambdaMaxAll(:,nPropConso) - lambdaMaxAll(:,moitie+1))./ (nConso(:,nPropConso) - nConso(:,moitie+1));

termeConstA = lambdaMaxAll(:,1) + penteA .* (0.5 *Nagent' - nConso(:,1));
termeConstB = lambdaMaxAll(:,nPropConso) + penteB .* (0.5 *Nagent' - nConso(:,nPropConso));


penteTermeConst = (termeConstB(nNagent) - termeConstB(1)) / (Nagent(nNagent) - Nagent(1));
CostTermeConst  = termeConstB(1) - penteTermeConst * Nagent(1);

figure()
plot(Nagent, -penteA, "DisplayName","pente opposée en phase A")
hold on
plot(Nagent, penteB, "DisplayName","pente en phase B")
legend()

figure()
plot(Nagent,termeConstA, "DisplayName","terme constant en phase A")
hold on
plot(Nagent,termeConstB, "DisplayName","terme constant en phase B")
legend()
%% Analyse via nConso lambdaMinAll
% fonction quadratique avec min au mileu z = 0.5x, on prend donc 2 points
% arbitrairement pour déterminer a et c (c n'est connnu que quand x est
% pair) à y = 0.1 et y = 0.4

a = (propConso(1)-0.5)^2;
b = (propConso(8)-0.5)^2;

A = (lambdaMinAll(:,1) - lambdaMinAll(:,8))./((a-b) * Nagent'.^2);
C = (lambdaMinAll(:,1) - (a/b) * lambdaMinAll(:,8))/(1 - (a/b));

figure()
plot(Nagent, C, "DisplayName","terme constant")
hold on
plot(Nagent, (C(1)*Nagent(1)) ./Nagent, "LineStyle","--", Marker="o", DisplayName="2.6/Nagent")
plot(Nagent, (C(nNagent)*Nagent(nNagent)) ./Nagent, "LineStyle","--", Marker="o", DisplayName="3.6/Nagent")
plot(Nagent, (1/3 * log(831*Nagent - 5755)) ./Nagent, "LineStyle","--", Marker="o", DisplayName="log(g(N))/3N")
legend()



figure()
plot(Nagent, exp(3*C'.*Nagent), "DisplayName","exp(3*C * Nagent)")
hold on
plot(Nagent, C'.*Nagent, "DisplayName","C * Nagent")
legend()


figure()
plot(Nagent, A, "DisplayName","coefficient directeur")
hold on
plot(Nagent, (A(1)*Nagent(1)) ./Nagent, "LineStyle","--", Marker="o", DisplayName="0.008/Nagent")
plot(Nagent, (A(nNagent)*Nagent(nNagent)) ./Nagent, "LineStyle","--", Marker="o", DisplayName="4.4e-5/Nagent")
plot(Nagent, (-0.00675 * Nagent + 0.9675)./(Nagent.^2), "LineStyle","--", Marker="o", DisplayName="g(N)/N^2")
legend()


figure()
plot(Nagent, A'.*Nagent.^2, "DisplayName","A*N^2")

%% Resultat

ApproLambdaMax = zeros(nNagent, nPropConso);
ApproLambdaMin = zeros(nNagent, nPropConso);

for i=(1:nNagent)
    for j=(1:nPropConso)
        N = Nagent(i);
        Y = nConso(i,j);

        ApproLambdaMax(i,j) = abs(Y - 0.5*N) + penteTermeConst * N + CostTermeConst;
        ApproLambdaMin(i,j) = (-0.00675*N + 0.9675)/N^2 * (Y-0.5*N)^2 + (log(831*N-5755))/(3*N);

    end
end

ErrLambdaMax = lambdaMaxAll - ApproLambdaMax;
ErrLambdaMin = lambdaMinAll - ApproLambdaMin;

figure('Units','centimeters','Position',[1 2 xSize ySize])
name = strcat('UtilitairePAC','lambdaMaxAllAppro');
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])
moitie = floor(nNagent/2) + 1;
for j=(1:moitie)
    plot(nConso(j,:), ApproLambdaMax(j,:), "DisplayName", "nAgent " + num2str(Nagent(j)), "LineStyle","--","Marker","o");
    hold on
end
for j =(moitie+1:nNagent)
   plot(nConso(j,:), ApproLambdaMax(j,:), "DisplayName", "nAgent " + num2str(Nagent(j)), "LineStyle","-.","Marker","+");
   hold on
end
legend("Location","best");
title("Valeur propre maximale approximée");
grid on
axeX = "Nombre de consomateur";
axeY = "Plus grande valeur propre";
xlabel(axeX,'FontSize',Fontsize_v)
ylabel(axeY,'FontSize',Fontsize_v)
hold off

print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])



figure('Units','centimeters','Position',[1 2 xSize ySize])
name = strcat('UtilitairePAC','lambdaMinAllAppro');
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])
moitie = floor(nNagent/2) + 1;
for j=(1:moitie)
    plot(nConso(j,:), ApproLambdaMin(j,:), "DisplayName", "nAgent " + num2str(Nagent(j)), "LineStyle","--","Marker","o");
    hold on
end
for j =(moitie+1:nNagent)
   plot(nConso(j,:), ApproLambdaMin(j,:), "DisplayName", "nAgent " + num2str(Nagent(j)), "LineStyle","-.","Marker","+");
   hold on
end
legend("Location","best");
title("Valeur propre minimale approximée");
grid on
axeX = "Nombre de consomateur";
axeY = "Plus grande valeur propre";
xlabel(axeX,'FontSize',Fontsize_v)
ylabel(axeY,'FontSize',Fontsize_v)
hold off

print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])
%%
figure('Units','centimeters','Position',[1 2 xSize ySize])
name = strcat('UtilitairePAC','lambdaMaxAllErr');
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])
moitie = floor(nNagent/2) + 1;
for j=(1:moitie)
    plot(nConso(j,:), ErrLambdaMax(j,:), "DisplayName", "nAgent " + num2str(Nagent(j)), "LineStyle","--","Marker","o");
    hold on
end
for j =(moitie+1:nNagent)
   plot(nConso(j,:), ErrLambdaMax(j,:), "DisplayName", "nAgent " + num2str(Nagent(j)), "LineStyle","-.","Marker","+");
   hold on
end
legend("Location","best");
title("Erreur sur la valeur propre maximale ");
grid on
axeX = "Nombre de consomateur";
axeY = "Erreur";
xlabel(axeX,'FontSize',Fontsize_v)
ylabel(axeY,'FontSize',Fontsize_v)
hold off

print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])

figure('Units','centimeters','Position',[1 2 xSize ySize])
name = strcat('UtilitairePAC','lambdaMinAllErr');
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])
moitie = floor(nNagent/2) + 1;
for j=(1:moitie)
    plot(nConso(j,:), ErrLambdaMin(j,:), "DisplayName", "nAgent " + num2str(Nagent(j)), "LineStyle","--","Marker","o");
    hold on
end
for j =(moitie+1:nNagent)
   plot(nConso(j,:), ErrLambdaMin(j,:), "DisplayName", "nAgent " + num2str(Nagent(j)), "LineStyle","-.","Marker","+");
   hold on
end
legend("Location","best");
title("Erreur sur la valeur propre minimale ");
grid on
axeX = "Nombre de consomateur";
axeY = "Erreur";
xlabel(axeX,'FontSize',Fontsize_v)
ylabel(axeY,'FontSize',Fontsize_v)
hold off

print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])

ErrRelLambdaMin = ErrLambdaMin ./ lambdaMinAll;
ErrRelLambdaMax = ErrLambdaMax ./ lambdaMaxAll;
max(abs(ErrLambdaMin), [], "all")
max(abs(ErrLambdaMax), [], "all")

max(abs(ErrRelLambdaMin), [], "all")
max(abs(ErrRelLambdaMax), [], "all")




