function [lambdaMax,lambdaMin, lambdaMin2] = findLambda(N,Ncons)
    %   findLambda find the eigth value to find optimal parameters of the
    %    PAC algorithm according to the number of agent and consumers
    Ncons = floor(Ncons);
    Ngen = N - Ncons;
    
    Nconst1 = 1 + Ngen;
    Nconst2 = 1 + Ncons;
    Nconst3 = Ngen * Nconst2 + Ncons * Nconst1;
    
    size1 = 1 + 2 * Ngen;
    size2 = 1 + 2 * Ncons;
    size3 = Ncons * size1 + Ngen * size2;
    
    Gglo = cell(N,1);
    Ghat = zeros(Nconst3,size3);
    B = zeros(2*Ncons*Ngen, size3);
    
    debutX = 1;
    debutY = 1;
        
    for i=(1:N)
        if(i<=Ncons)
            Gglo{i} = zeros(Nconst1,size1);
            Gglo{i}(1,2:1+Ngen) = -1;
            for j=(1:Ngen)
                B((i-1)*Ngen + j, debutX + j + Ngen) = 1;
                B((i-1)*Ngen + j, Ncons*size1 + (j - 1)*size2 + i + 1) = -1;
                Gglo{i}(1 + j, 1 + j) = 1;
                Gglo{i}(1 + j, 1 + j + Ngen)= 1;
            end
            offsetX = size1;
            offsetY = Nconst1;
        else
            Gglo{i} = zeros(Nconst2,size2);
            Gglo{i}(1,2:1 + Ncons) = -1;
            for j=(1:Ncons)
                 B(Ncons * Ngen + (i - Ncons -1)*Ncons + j, debutX + j + Ncons) = 1;
                 B(Ncons * Ngen + (i - Ncons -1)*Ncons + j, (j - 1) * size1 + (i - Ncons) + 1) = -1;
                 Gglo{i}(1 + j, 1 + j ) = 1;
                 Gglo{i}(1 + j, 1 + j + Ncons)= 1;
            end
            offsetX = size2;
            offsetY = Nconst2;
        end
        Gglo{i}(1,1) = 1;
        
    
    
        Ghat(debutY:debutY+offsetY-1,debutX:debutX+offsetX-1) =  Gglo{i};
        debutX = debutX + offsetX;
        debutY = debutY + offsetY;
    end

    H = Ghat' * Ghat + B'*B;
    %clear Ghat;
    %clear B;
    lambda = eig(H);
    lambdaMax = max(lambda);
    epsilon = 0.000001;
    lambdahat = lambda(lambda>epsilon);
    lambdaMin = min(lambdahat);
    lambdahat2 = lambda(lambda~=0);
    lambdaMin2 = min(lambdahat2);

end