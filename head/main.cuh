#pragma once




// utilities
#include "MatrixCPU.h"
#include "MatrixGPU.cuh"
#include "StudyCase.h"
#include "Simparam.h"
#include "System.h"

//Market
#include "ADMMMarket.h"
#include "ADMMMarketOpenMP.h"
#include "OSQP.h" 
#include "PAC.h" 
#include "PACOpenMP.h" 
#include "PACGPU.cuh"
#include "ADMMMarketGPU.cuh"

#include <stdio.h>
#include <iostream>
#include <time.h>
#include <cuda_runtime.h>
#include <osqp.h>
#include <cudaProfiler.h>






void testMarket();
void SimuCompare();

//utilitaire
float pow10(int n);
int getNFileline(std::string nameFile);
std::string generateDate(int year, int month, int day, int hour);

/*
On ne peut inclure qu'un seul osqp � la fois, (car m�me nom)
C:\Program Files \OSQP\osqp\include\osqp


*/