
#include "../head/main.cuh"


// mingw32 - make.exe

// Simulation

int main(int argc, char* argv[]) {

	srand(time(nullptr));
	std::cout.precision(6);
	
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	try
	{
		SimuCompare();
		//testMarket();
	}
	catch (const std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
	
	return 0;
}


/* Simu marche*/

void SimuCompare()
{
	std::string fileName = "ComparaisonMarket_JCGE_20_v2.csv";
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	const int nMethode = 6;
	std::vector<int> indices = { 0, 1, 2, 3, 4, 5};
	//std::string methodesName[nMethode] = { "OSQP", "PAC", "ADMMMarket"};
	std::string methodesName[nMethode] = { "ADMMMarket", "ADMMMaketOpenMP", "ADMMGPU", "PAC","PACOpenMP","PACGPU" };
	Method* methodes[nMethode];
	/*methodes[0] = new OSQP;
	methodes[1] = new PAC;
	methodes[2] = new ADMMMarket;*/
	methodes[0] = new ADMMMarket;
	methodes[1] = new ADMMMarketOpenMP;
	methodes[2] = new ADMMMarketGPU;
	methodes[3] = new PAC;
	methodes[4] = new PACOpenMP;
	methodes[5] = new PACGPU;

	if (nMethode != indices.size()) {
		throw std::domain_error("not good number of methods");
	}

	float P = 100;
	float dP = 20;
	float Q = 10;
	float dQ = 2;
	float a = 0.07; // pour P et Q
	float da = 0.02;
	float b = 10;
	float db = 4;
	float gamma = 8;
	float dGamma = 2;
	float propCons = 0.375f;
	float propPro = 0;
	float propGenNFle = 0.125f;
	bool AC = true;
	

	int nNAgent = 1;
	int nAgentMax = 20;
	int offset = 0;
	float rhoAgent = 0.025;
	int nSimu = 50;
	int iterGlobal = 50;
	int iterLocal = 1000;
	int stepG = 10;
	int stepL = 10;
	float epsG = 0.01f;
	float epsL = 0.001f;

	std::chrono::high_resolution_clock::time_point t1;
	std::chrono::high_resolution_clock::time_point t2;
	int million = 1000000;

	MatrixCPU Param(1, 25);
	Param.set(0, 0, nAgentMax);
	Param.set(0, 1, nNAgent);
	Param.set(0, 2, rhoAgent);
	Param.set(0, 3, epsG);
	Param.set(0, 4, epsL);
	Param.set(0, 5, iterGlobal);
	Param.set(0, 6, iterLocal);
	Param.set(0, 7, stepG);
	Param.set(0, 8, stepL);
	Param.set(0, 9, nSimu);
	Param.set(0, 10, nMethode);
	Param.set(0, 11, P);
	Param.set(0, 12, dP);
	Param.set(0, 13, Q);
	Param.set(0, 14, dQ);
	Param.set(0, 15, a);
	Param.set(0, 16, da);
	Param.set(0, 17, b);
	Param.set(0, 18, db);
	Param.set(0, 19, gamma);
	Param.set(0, 20, dGamma);
	Param.set(0, 21, propCons);
	Param.set(0, 22, propPro);
	Param.set(0, 23, propGenNFle);
	Param.set(0, 24, AC);

	Param.saveCSV(fileName, mode);

	MatrixCPU Agents(1, nNAgent);
	MatrixCPU temps(nMethode * nSimu, nNAgent, nanf(""));
	MatrixCPU iters(nMethode * nSimu, nNAgent, nanf(""));
	MatrixCPU fcs(nMethode * nSimu, nNAgent, nanf(""));
	MatrixCPU ResF(2, iterGlobal / stepG);
	MatrixCPU ResR(nMethode * nSimu, nNAgent, nanf(""));
	MatrixCPU ResS(nMethode * nSimu, nNAgent, nanf(""));
	System sys;
	StudyCase cas;
	sys.setIter(iterGlobal, iterLocal);
	sys.setEpsG(epsG);
	sys.setEpsL(epsL);
	sys.setStep(stepG, stepL);


	for (int agent = 0; agent < nNAgent; agent++) {
		std::cout << "--------- --------- --------- --------- ----------" << std::endl;

		int agents = (agent + 1) * (nAgentMax - offset) / nNAgent + offset;
		std::cout << agents << std::endl;

		Agents.set(0, agent, agents);

		for (int j = 0; j < nSimu; j++) {
			if (AC) {
				cas = StudyCase(agents, P, dP, P, dP, a, da, a, da, b, db, gamma, dGamma, propCons, propGenNFle, propPro);
			}
			else
			{
				cas = StudyCase(agents, P, dP, a, da, b, db, propCons, propPro);
			}
			sys.setStudyCase(cas);
			std::cout << "-";
			float rho = rhoAgent * agents;
			sys.setRho(rho);
			std::random_shuffle(indices.begin(), indices.end());
			for (int i = 0; i < nMethode; i++) {
				if (indices[i] > 2) {
					methodes[indices[i]]->setBestParam(cas);
				}

				sys.setMethod(methodes[indices[i]]);
				
			
				//clock_t t = clock();
				t1 = std::chrono::high_resolution_clock::now();
				Simparam res = sys.solve();
				t2 = std::chrono::high_resolution_clock::now();
				//clock_t temp = clock() - t;
				int iter = res.getIter();
				temps.set(indices[i] * nSimu + j, agent, (float)std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() / million);
				iters.set(indices[i] * nSimu + j, agent, iter);
				fcs.set(indices[i] * nSimu + j, agent, res.getFc());
				ResF = res.getRes();
				ResR.set(indices[i] * nSimu + j, agent, ResF.get(0, (iter - 1) / stepG));
				ResS.set(indices[i] * nSimu + j, agent, ResF.get(1, (iter - 1) / stepG));
				sys.resetParam();
			}
		}
		std::cout << std::endl;
	}
	Agents.saveCSV(fileName, mode);
	temps.saveCSV(fileName, mode);
	iters.saveCSV(fileName, mode);
	fcs.saveCSV(fileName, mode);
	ResR.saveCSV(fileName, mode);
	ResS.saveCSV(fileName, mode);

	/*Agents.display();
	temps.display();
	iters.display();
	fcs.display();
	ResR.display();
	ResS.display();*/

	for (int i = 0; i < nMethode; i++) {
		DELETEB(methodes[i]);
	}
	sys.setMethod(nullptr);
}

/* Test fonctionnel */

void testMarket()
{
	StudyCase cas;
	int million = 1000000;
	int choseCase = 2;
	std::string fileName = "TimeByBlockPF";
	std::string chosenCase = "";
	float Power = 0;
	int nMethode = 7;
	bool methodeToSimule[9] = { false, false, false, false, false, false, true, false, false };

	int method = 0;
	std::chrono::high_resolution_clock::time_point t1;
	std::chrono::high_resolution_clock::time_point t2;

	// for random case
	float P = 100;
	float dP = 10;
	float a = 0.07;
	float da = 0.02;

	float b = 5;
	float db = 2;
	int agents = 5;
	float beta = 10;
	float dBeta = 2;
	float propGen = 0.5;
	float propPro = 0;

	bool AC = false;

	// for european case
	int nCons = 1494;
	std::string path = "data/";
	int Nhour = 24 * 31; // janvier
	MatrixCPU P0Global(nCons, Nhour);
	MatrixCPU P0(nCons, 1);
	std::string nameP0 = path + "/load/Month/2012-01.txt";
	P0Global.setFromFile(nameP0, 1);
	P0Global.getBloc(&P0, 0, nCons, 0, 1); // 1ere heure de l'ann�e
	
	float rho = 125;
	float factor = 0.9;


	switch (choseCase)
	{
	case 0:
		if (AC) {
			cas.SetAC2node();
		}
		else {
			cas.Set2node();
			nMethode += 2;
			cas.setReduce(true);
		}
		cas.display();
		break;
	case 1:
		if (AC) {
			chosenCase = "case118"; //case10ba case4_dist case85
			cas.SetACFromFile(chosenCase);
		}
		else
		{
			cas.Set29node();
			nMethode += 2;
			cas.setReduce(true);
		}
		cas.display();
		break;
	case 2:
		if (AC) {
			cas.SetEuropeTestFeeder();
		}
		else {
			cas.SetEuropeP0WithoutConstraint(path, &P0);
			nMethode += 2;
			cas.setReduce(true);
		}
		//cas.display();
		break;
	case 3:
		if (AC) {
			cas.SetAC3Bus();
		}
		else
		{
			cas.Set3Bus();
			nMethode += 2;
			cas.setReduce(true);
		}
		cas.display();
		break;
	case 4:
		if (AC) {
			cas = StudyCase(agents, P, dP, P, dP, a, da, a, da, b, db, beta, dBeta);
		}
		else
		{
			cas = StudyCase(agents, P, dP, a, da, b, db, propGen, propPro);
			nMethode += 2;
			cas.setReduce(true);
		}
		cas.display();
		break;
	default:
		throw std::invalid_argument("unknown choseCase");
		break;
	}
	MatrixCPU results(5, nMethode, -1);
	ADMMMarket admmMarket;
	ADMMMarketOpenMP admmMarketOpenMP;
	ADMMMarketGPU admmMarketGPU;
	OSQP osqp;
	PAC pac;
	PACGPU pacGPU;
	PACOpenMP pacOpenMP;
	ADMMConst1 admmMarketEndo;
	ADMMGPUConst4 admmMarketEndoGPU;

	Simparam param(cas.getNagent(), cas.getNLine(true), cas.getNLine(), AC);
	float epsL = 0.0005;
	param.setEpsL(epsL);
	param.setEpsG(0.001f);
	param.setItG(50000); //500000
	param.setItL(1000);
	param.setStep(10, 5);
	

	param.setRho(rho);
	
	Simparam res(param);
	int nAgent = cas.getNagent();
	MatrixCPU Pn;
	/*param.display(1);*/
	std::cout << "**************************     ADMMMarket      ****************************************" << std::endl;

	if (methodeToSimule[method]) {
		t1 = std::chrono::high_resolution_clock::now();
		admmMarket.solve(&res, param, cas);
		t2 = std::chrono::high_resolution_clock::now();
		Pn = res.getPn();
		Pn.display();


		results.set(0, method, Pn.get(1, 0));
		if (AC) {
			results.set(1, method, Pn.get(nAgent + 1, 0));
		}
		results.set(2, method, res.getFc());
		results.set(3, method, (float)std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() / million);
		results.set(4, method, res.getIter());

		res.display();
	}
	

	method++;
	std::cout << "***************************** admmMarketOpenMP     ****************************************" << std::endl;

	if (methodeToSimule[method]) {
		t1 = std::chrono::high_resolution_clock::now();
		admmMarketOpenMP.solve(&res, param, cas);
		t2 = std::chrono::high_resolution_clock::now();
		Pn = res.getPn();
		Pn.display();

		results.set(0, method, Pn.get(1, 0));
		if (AC) {
			results.set(1, method, Pn.get(nAgent + 1, 0));
		}
		results.set(2, method, res.getFc());
		results.set(3, method, (float)std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() / million);
		results.set(4, method, res.getIter());



		res.display();
	}
	method++;
	if (method > (nMethode + 1)) {
		throw std::invalid_argument("nMethod is too small");
	}
	std::cout << "********************************** admmMarketGPU **************************************" << std::endl;

	if (methodeToSimule[method]) {
		t1 = std::chrono::high_resolution_clock::now();
		admmMarketGPU.solve(&res, param, cas);
		t2 = std::chrono::high_resolution_clock::now();
		Pn = res.getPn();
		Pn.display();

		results.set(0, method, Pn.get(1, 0));
		if (AC) {
			results.set(1, method, Pn.get(nAgent + 1, 0));
		}
		results.set(2, method, res.getFc());
		results.set(3, method, (float)std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() / million);
		results.set(4, method, res.getIter());



		res.display();
		
	}
	method++;
	if (method > (nMethode + 1)) {
		throw std::invalid_argument("nMethod is too small");
	}

	std::cout << "********************************** OSQP  *****************************************" << std::endl;

	if (methodeToSimule[method]) {
		t1 = std::chrono::high_resolution_clock::now();
		osqp.solve(&res, param, cas);
		t2 = std::chrono::high_resolution_clock::now();
		Pn = res.getPn();
		Pn.display();

		results.set(0, method, Pn.get(1, 0));
		if (AC) {
			results.set(1, method, Pn.get(nAgent + 1, 0));
		}
		results.set(2, method, res.getFc());
		results.set(3, method, (float)std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() / million);
		results.set(4, method, res.getIter());



		res.display();
	}
	method++;
	if (method > (nMethode)) {
		throw std::invalid_argument("nMethod is too small");
	}




	std::cout << "******************************       PAC     ***************************************" << std::endl;

	if (methodeToSimule[method]) {
		pac.setBestRhoGammaHeuristic(cas);
		t1 = std::chrono::high_resolution_clock::now();
		pac.solve(&res, param, cas);
		t2 = std::chrono::high_resolution_clock::now();
		Pn = res.getPn();
		Pn.display();

		results.set(0, method, Pn.get(1, 0));
		if (AC) {
			results.set(1, method, Pn.get(nAgent + 1, 0));
		}
		results.set(2, method, res.getFc());
		results.set(3, method, (float)std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() / million);
		results.set(4, method, res.getIter());



		res.display();
	}
	method++;
	if (method > (nMethode)) {
		throw std::invalid_argument("nMethod is too small");
	}
	std::cout << "******************************       PAC OpenMP    ***************************************" << std::endl;

	if (methodeToSimule[method]) {
		pacOpenMP.setBestRhoGammaHeuristic(cas);
		t1 = std::chrono::high_resolution_clock::now();
		pacOpenMP.solve(&res, param, cas);
		t2 = std::chrono::high_resolution_clock::now();
		Pn = res.getPn();
		Pn.display();

		results.set(0, method, Pn.get(1, 0));
		if (AC) {
			results.set(1, method, Pn.get(nAgent + 1, 0));
		}
		results.set(2, method, res.getFc());
		results.set(3, method, (float)std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() / million);
		results.set(4, method, res.getIter());



		res.display();
	}
	method++;
	if (method > (nMethode)) {
		throw std::invalid_argument("nMethod is too small");
	}

	std::cout << "******************************       PAC GPU     ***************************************" << std::endl;

	if (methodeToSimule[method]) {
		pacGPU.setBestRhoGammaHeuristic(cas);
		t1 = std::chrono::high_resolution_clock::now();
		pacGPU.solve(&res, param, cas);
		t2 = std::chrono::high_resolution_clock::now();
		Pn = res.getPn();
		Pn.display();

		results.set(0, method, Pn.get(1, 0));
		if (AC) {
			results.set(1, method, Pn.get(nAgent + 1, 0));
		}
		results.set(2, method, res.getFc());
		results.set(3, method, (float)std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() / million);
		results.set(4, method, res.getIter());



		res.display();
	}
	method++;
	if (method > (nMethode)) {
		throw std::invalid_argument("nMethod is too small");
	}

	std::cout << "********************************  admmMarketEndo  ***************************************" << std::endl;

	if (!AC && methodeToSimule[method]) {

		t1 = std::chrono::high_resolution_clock::now();
		//admmMarketEndo.solve(&res, param, cas);
		t2 = std::chrono::high_resolution_clock::now();
		Pn = res.getPn();
		Pn.display();
		results.set(0, method, Pn.get(1, 0));
		if (AC) {
			results.set(1, method, Pn.get(nAgent + 1, 0));
		}
		results.set(2, method, res.getFc());
		//results.set(3, method, (float)std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() / million);
		//results.set(4, method, res.getIter());



		res.display();
		
		
		if (method + 1 > (nMethode)) {
			throw std::invalid_argument("nMethod is too small");
		}
	}
	method++;
	
	std::cout << "**********************************   admmMarketEndoGPU    *******************************************" << std::endl;

	if (!AC && methodeToSimule[method]) {

		t1 = std::chrono::high_resolution_clock::now();
		//admmMarketEndoGPU.solve(&res, param, cas);
		t2 = std::chrono::high_resolution_clock::now();
		Pn = res.getPn();
		Pn.display();

		results.set(0, method, Pn.get(1, 0));
		if (AC) {
			results.set(1, method, Pn.get(nAgent + 1, 0));
		}
		results.set(2, method, res.getFc());
		//results.set(3, method, (float)std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() / million);
		//results.set(4, method, res.getIter());



		res.display();
		
		if (method + 1 > (nMethode)) {
			throw std::invalid_argument("nMethod is too small");
		}

	}
	method++;
	std::cout << "*****************************************************************************" << std::endl;
	std::cout << " ADMMMarket , ADMMMarketOpenMP, ADMMMarketGPU, OSQP, PAC, PAC OpenMp, PACGPU ";
	if (!AC) {
		std::cout << " admmMarketEndo , admmMarketEndoGPU ";
	}
	std::cout << std::endl;


	results.display();
}



/*  autre */

std::string generateDate(int year, int month, int day, int hour)
{
	std::string smonth;
	std::string sday;
	std::string shour;
	if (month < 10) {
		smonth = "0" + std::to_string(month);
	}
	else {
		smonth = std::to_string(month);
	}
	if (day < 10) {
		sday = "0" + std::to_string(day);
	}
	else {
		sday = std::to_string(day);
	}
	if (hour < 10) {
		shour = "0" + std::to_string(hour);
	}
	else {
		shour = std::to_string(hour);
	}
		
		

	std::string d = std::to_string(year) + "-" + smonth + "-" + sday + " " + shour +"-00-00";
	
	return d;
}

float pow10(int n)
{
	float v = 1;
	for (int i = 0; i < n; i++) {
		v = v * 10;
	}
	return v;
}

int getNFileline(std::string nameFile)
{
	int number_of_lines = 0;
	std::string line;
	std::ifstream myfile(nameFile);

	while (std::getline(myfile, line))
		++number_of_lines;
	return number_of_lines;
}

