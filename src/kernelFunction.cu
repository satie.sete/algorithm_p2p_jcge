
#include "../head/kernelFunction.cuh"


// OPF ADMM

__global__ void updateQ(float* Q, float* X, float* MU, float _rho, int sizeOPF) {

	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int step = blockDim.x * gridDim.x;


	for (int j = index; j < sizeOPF; j += step) {
		Q[j] = -(MU[j] + _rho * X[j]); //Q[i].set(j, 0, -(Mu[i].get(j, 0) + _rho * X[i].get(j, 0)));
	}
}


__global__ void updateMUGPU(float* Mu, float* Y, float* X, float rho, int sizeOPF) {
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int step = blockDim.x * gridDim.x;


	for (int j = index; j < sizeOPF; j += step) {
		Mu[j] = Mu[j] + rho * (X[j] - Y[j]);
	}
}

__global__ void removeLossAgent(float* _nAgentByBus, float* CoresAgentBusBegin) {

	int thIdx = threadIdx.x;
	if (thIdx == 0) {
		_nAgentByBus[0] = _nAgentByBus[0] - 1;
		CoresAgentBusBegin[0] = 1;
	}
}


__global__ void initVoltageBound(float* VlimReal, float* Vlim, float* constraintLo, float* constraintUp, float* nChild, int nBus) {

	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int step = blockDim.x * gridDim.x;

	for (int b = index; b < nBus; b += step) {
		float nb = sqrtf((nChild[b] + 1) / 2);
		float ub = constraintUp[b + nBus];
		float lb = constraintLo[b + nBus];

		VlimReal[b] = lb;
		VlimReal[b + nBus] = ub;

		Vlim[b] = lb * lb * nb;
		Vlim[b + nBus] = ub * ub * nb;
	}
}


// FIN OPF ADMM
