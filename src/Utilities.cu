

#include "../head/Utilities.cuh"
#define PI 3.14159265359



template <typename T>
void check(T err, const char* const func, const char* const file,
	const int line)
{
	if (err != cudaSuccess)
	{
		std::cerr << "CUDA Runtime Error at: " << file << ":" << line
			<< std::endl;
		std::cerr << cudaGetErrorString(err) << " " << func << std::endl;
		// We don't exit when we encounter CUDA errors in this example.
		// std::exit(EXIT_FAILURE);
	}
}


void checkLast(const char* const file, const int line)
{
	cudaError_t err{ cudaGetLastError() };
	if (err != cudaSuccess)
	{
		std::cerr << "CUDA Runtime Error at: " << file << ":" << line
			<< std::endl;
		std::cerr << cudaGetErrorString(err) << std::endl;
		// We don't exit when we encounter CUDA errors in this example.
		// std::exit(EXIT_FAILURE);
	}
}






int resolveRealPolynome3without2term(double* root, double* coef) {
	/*
	* return : the number of real root for the polynome x^3 + px + q = 0
	* root : is a array of size 3
	* coeff : is a array of size 2 (p and q of x^3 + px + q = 0)
	* must add -b/3a if bx^2 is not null at the begining and coefPolynome3From4to2coef is used
	*/
	double p = coef[0];
	double q = coef[1];
	double Delta = -4 * p*p*p - 27 *q*q;
	if (Delta == 0) {
		root[0] = -3 * q / (2 * p);
		root[1] = -2 * root[0];
		return 2;
	}
	else if (Delta < 0) {

		root[0] = cbrt((-q + sqrt(-Delta / 27)) / 2) + cbrt((-q - sqrt(-Delta / 27)) / 2);

		return 1;
	}
	else {
		double r = (3 * q * sqrt(3)) / (2 * p * sqrt(-p)) ;
		for (int k = 0; k < 3; k++) {
			root[k] = 2 * sqrt(-p / 3) * cos((acos(r) + 2 * k * PI) / 3);
		}

		return 3;
	}
}

int resvolveRealPolynome4without2term(double* root, double* coef)
{
	/*
	* return : the number of real root for the polynome x^4 + bx^3 + dx + e = 0
	* root : is a array of size 4
	* coeff : is a array of size 3 (b,d,e)
	*/
	double b = coef[0];
	double d = coef[1];
	double e = coef[2];
	int nRoot = 0;

	if (b * b * b + 8 * d == 0) {
		//if (abs(b * b * b + 8 * d) < 0.00000001) {
		// passage de p^4 + b p^3 + d p + e -> a p^4 + b p^2 + c = 0
		double B = -3 * b * b / 8;
		double C = -3* b*b*b*b/256 - b*d/4 + e;

		double delta = B * B - 4 * C;
		//std::cout << "Delta " << delta;
		if (delta  == 0) {
			double z = - B / 2;
			nRoot = 2;
			//std::cout << " z " << z << std::endl;
			root[0] = sqrt(z);
			root[1] = -sqrt(z);
			return nRoot;
		}
		else if (delta > 0) {
			double z1 = (-B + sqrt(delta)) / 2;
			double z2 = (-B - sqrt(delta)) / 2;
			//std::cout << " z1 " << z1 << " z2 " << z2 << std::endl;
			nRoot = 4;
			if (z1 >= 0) {
				root[0] = sqrt(z1);
				root[1] = -sqrt(z1);
				nRoot = 2;
			} if (z2 >= 0) {
				root[nRoot] = sqrt(z2);
				root[nRoot + 1] = -sqrt(z2);
				nRoot += 2;
			}
			return nRoot;
		}
		else { // delta < 0
			//std::cout << "pas de racines r�elle !!!! rip, on tente le pas bicarr�" << std::endl;
		}
	}

	// for the lambda polynome
	double coef2[2];
	double rootlambda[3];
	coef2[0] = (2 * b * d - 8 * e) / 8;
	coef2[1] = -(b * b * e + d * d) / 8;
	int nRootlambda = resolveRealPolynome3without2term(rootlambda, coef2);


	
	
	for (int i = 0; i < nRootlambda; i++) {
		double lambda0 = rootlambda[i];
		//std::cout << "poly3 " << coef2[0] * lambda0 + coef2[1] + lambda0 * lambda0 * lambda0 << std::endl;
		double mu1 = 2 * lambda0 + (b * b) / 4;
		if (mu1 > 0) {
			double mu0 = sqrt(mu1);
			double DeltaP = -2 * lambda0 + 2 * (d - b * lambda0) / mu0 + b * mu0 + b * b / 2;
			double DeltaM = -2 * lambda0 - 2 * (d - b * lambda0) / mu0 - b * mu0 + b * b / 2;
			if (DeltaP >= 0) {
				root[nRoot] = (-mu0 + sqrt(DeltaP)) / 2 - b / 4;
				root[nRoot + 1] = (-mu0 - sqrt(DeltaP)) / 2 - b / 4;
				nRoot = nRoot + 2;
				//std::cout << "  Dp   ";
			}
			if (DeltaM >= 0) {
				root[nRoot] = (mu0 + sqrt(DeltaM)) / 2 - b / 4;
				root[nRoot + 1] = (mu0 - sqrt(DeltaM)) / 2 - b / 4;
				nRoot = nRoot + 2;
				//std::cout << "  DM   ";
			}
			if (nRoot > 0) {
				//std::cout << "poly4 " << coef[0] << " " <<  coef[1] << " " << coef[2] << std::endl;
				return nRoot;
			}
		}
		
	}
	double lambda0 = rootlambda[0];
	double mu0 = sqrt(2 * lambda0 + (b * b) / 4);
	double DeltaP = -2 * lambda0 + 2 * (d - b * lambda0) / mu0 + b * mu0 + b * b / 2;
	double DeltaM = -2 * lambda0 - 2 * (d - b * lambda0) / mu0 - b * mu0 + b * b / 2;
	std::cout << "no real root found " << abs(b * b * b + 8 * d) << " " << lambda0 << " " << mu0 << " " << DeltaP << " " << DeltaM << std::endl;
	
	return nRoot;
}

void coefPolynome3From4to2coef(double* coef4, double* coef2)
{

	double a = coef4[0];

	if (a == 0) {
		throw std::invalid_argument("must be a thrid degree polynome, a!=0 ");
	}

	double b = coef4[1];
	double c = coef4[2];
	double d = coef4[3];

	coef2[0] = (3 * a * c - b * b) / (3 * a * a);
	coef2[1] = (2 * b * b * b - 9 * a * b * c + 27 * a * a * d) / (27 * a * a * a);

}

// IDE compl�tement paum�....

__device__ int resolveRealPolynome3without2termGPU(double* root, double p, double q) {

	double Delta = -4 * p * p * p - 27 * q * q;
	if (Delta == 0) {
		root[0] = -3 * q / (2 * p);
		root[1] = -2 * root[0];
		return 2;
	}
	else if (Delta < 0) {

		root[0] = cbrt((-q + sqrtf(-Delta / 27)) / 2) + cbrt((-q - sqrtf(-Delta / 27)) / 2);

		return 1;
	}
	else {

		for (int k = 0; k < 3; k++) {
			double r = (3 * q * sqrtf(3)) / (2 * p * sqrtf(-p));
			r = -(r + 1) * (r < -1) + (1 - r) * (r > 1) + r;
			root[k] = 2 * sqrtf(-p / 3) * cos((acos(r) + 2 * k * PI) / 3);
		}

		return 3;
	}

}/**/

__device__ int resvolveRealPolynome4without2termGPU(double* root, double b, double d, double e)
{
	/*
	* return : the number of real root for the polynome x^4 + bx^3 + dx + e = 0
	* root : is a array of size 4
	* coeff : is a array of size 3 (b,d,e)
	*/
	int nRoot = 0;

	if (b * b * b + 8 * d == 0) {

		// passage de p^4 + b p^3 + d p + e -> a p^4 + b p^2 + c = 0
		double B = -3 * b * b / 8;
		double C = -3 * b * b * b * b / 256 - b * d / 4 + e;

		double delta = B * B - 4 * C;
		//std::cout << "Delta " << delta;
		if (delta == 0) {
			double z = -B / 2;
			nRoot = 2;
			//std::cout << " z " << z << std::endl;
			root[0] = sqrtf(z);
			root[1] = -sqrtf(z);
			return nRoot;
		}
		else if (delta > 0) {
			double z1 = (-B + sqrtf(delta)) / 2;
			double z2 = (-B - sqrtf(delta)) / 2;
			//std::cout << " z1 " << z1 << " z2 " << z2 << std::endl;
			nRoot = 4;
			if (z1 >= 0) {
				root[0] = sqrtf(z1);
				root[1] = -sqrtf(z1);
				nRoot = 2;
			} if (z2 >= 0) {
				root[nRoot] = sqrtf(z2);
				root[nRoot + 1] = -sqrtf(z2);
				nRoot += 2;
			}
			return nRoot;
		}
		else { // delta < 0
			//std::cout << "pas de racines r�elle !!!! rip, on tente le pas bicarr�" << std::endl;
		}
	}


	double rootlambda[3];
	double coef2_0 = (2 * b * d - 8 * e) / 8;
	double coef2_1 = -(b * b * e + d * d) / 8;
	int nRootlambda = resolveRealPolynome3without2termGPU(rootlambda, coef2_0, coef2_1);

	for (int i = 0; i < nRootlambda; i++) {
		double lambda0 = rootlambda[i];
		double mu1 = 2 * lambda0 + (b * b) / 4;
		if (mu1 > 0) {
			double mu0 = sqrtf(mu1);
			double DeltaP = -2 * lambda0 + 2 * (d - b * lambda0) / mu0 + b * mu0 + b * b / 2;
			double DeltaM = -2 * lambda0 - 2 * (d - b * lambda0) / mu0 - b * mu0 + b * b / 2;
			if (DeltaP >= 0) {
				root[nRoot] = (-mu0 + sqrtf(DeltaP)) / 2 - b / 4;
				root[nRoot + 1] = (-mu0 - sqrtf(DeltaP)) / 2 - b / 4;
				nRoot = nRoot + 2;
			}
			if (DeltaM >= 0) {
				root[nRoot] = (mu0 + sqrtf(DeltaM)) / 2 - b / 4;
				root[nRoot + 1] = (mu0 - sqrtf(DeltaM)) / 2 - b / 4;
				nRoot = nRoot + 2;
			}
			if (nRoot > 0) {
				return nRoot;
			}
		}
	}
	return nRoot;
}

__device__ void coefPolynome3From4to2coefGPU(double* coef4, double* coef2) {
	double a = coef4[0];

	double b = coef4[1];
	double c = coef4[2];
	double d = coef4[3];

	coef2[0] = (3 * a * c - b * b) / (3 * a * a);
	coef2[1] = (2 * b * b * b - 9 * a * b * c + 27 * a * a * d) / (27 * a * a * a);
}

__global__ void resolveSeveralRealPolynome3termGPU(double* nRoot, double* roots, double* coefs, int nPoly) {
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int step = blockDim.x * gridDim.x;

	for (int i = index; i < nPoly; i += step) {
		double coefsLocal2[2];
		double rootsLocal[3];
		double coefsLocal4[4];
		int nRootLocal = 0;
		for (int j = 0; j < 4; j++) {
			coefsLocal4[j] = coefs[j * nPoly + i];
		}
		if (coefsLocal4[0] == 0) { // polynone of degre 2
			if (coefsLocal4[1] == 0) { // polynom of degre 1
				if (coefsLocal4[2] != 0) { // no const
					nRootLocal = 1;
					rootsLocal[0] = -coefsLocal4[3] / coefsLocal4[2];
				}
			}
			else {
				// la flemme WIP
			}
		}
		else {
			
			
			coefPolynome3From4to2coefGPU(coefsLocal4, coefsLocal2);
			
			

			nRootLocal = resolveRealPolynome3without2termGPU(rootsLocal, coefsLocal2[0], coefsLocal2[1]);
			for (int k = 0; k < nRootLocal; k++) {
				rootsLocal[k] += -coefsLocal4[1] / (3 * coefsLocal4[0]);
			}
		}
		for (int j = 0; j < nRootLocal; j++) {
			roots[j * nPoly + i] = rootsLocal[j];
		}
		nRoot[i] = nRootLocal;

	}
}


__global__ void resolveSeveralRealPolynome4WO2termGPU(double* nRoot, double* roots, double* coefs, int nPoly) {
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int step = blockDim.x * gridDim.x;

	for (int i = index; i < nPoly; i += step) {
		double rootsLocal[4];
		double coefsLocal3[3];
		int nRootLocal = 0;
		for (int j = 0; j < 4; j++) {
			coefsLocal3[j] = coefs[j * nPoly + i];
		}
		
		nRootLocal = resvolveRealPolynome4without2termGPU(rootsLocal, coefsLocal3[0], coefsLocal3[1], coefsLocal3[2]);
		
		
		for (int j = 0; j < nRootLocal; j++) {
			roots[j * nPoly + i] = rootsLocal[j];
		}
		nRoot[i] = nRootLocal;

	}
}